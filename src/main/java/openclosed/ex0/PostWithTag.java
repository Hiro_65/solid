package openclosed.ex0;

import single.responsibility.cohension.ex3.Database;

public class PostWithTag extends Post{
    public void createPostWithTag(Database db, String postMessage) {
        db.add(postMessage);
        db.addAsTag(postMessage);
    }
}
