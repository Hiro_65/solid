package openclosed.ex0;

import single.responsibility.cohension.ex3.Database;

public class Post implements IPost {
    public void createPost(Database db, String postMessage) {
        db.add(postMessage);
    }
}
