package kiss;

import java.util.HashMap;
import java.util.Map;

public class IntegrationHandlerFactory {
    public IntegrationHandler getHandlerFor(String integration) {
        Map<String, IntegrationHandler> handlerMap = new HashMap<>();
        handlerMap.put(EmailIntegrationHandler.EMAIL, new EmailIntegrationHandler());
        handlerMap.put(SMSIntegrationHandler.SMS, new SMSIntegrationHandler());
        handlerMap.put(PushIntegrationHandler.PUSH, new PushIntegrationHandler());
        if (handlerMap.containsKey(integration)){
            return handlerMap.get(integration);
        } else {
            throw new IllegalArgumentException("No handler found for integration: " + integration);
        }
    }
}