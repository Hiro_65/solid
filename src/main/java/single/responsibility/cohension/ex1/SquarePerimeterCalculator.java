package single.responsibility.cohension.ex1;

public class SquarePerimeterCalculator {
    int size = 5;
    public int calculateP() {
        return size * 4;
    }
}
