package single.responsibility.cohension.ex3;

class Post {
    public void CreatePost(Database db, String postMessage) {
        try {
            db.add(postMessage);
        } catch (Exception ex) {
            Error.logError("An error occurred: ", ex.toString());
            FileUtil.writeAllText("LocalErrors.txt", ex.toString());
        }
    }
}