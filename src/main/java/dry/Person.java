package dry;

class Person {
    String name;
    int age;
    Gender sex;
    HairColor hairColor;
    SkinColor skinColor;
}

enum SkinColor {
    BLACK, BROWN, WHITE, YELLOW
}

enum HairColor {
    RED, BLACK, YELLOW
}

enum Gender {
    FEMALE,
    MALE,
    GAY,
    LES,
}