package dry;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class GoodParents extends Person{
    public List<Person> findAllGoodParents(List<Person> personList) {
        List<Person> goodParents = new ArrayList<>();
        for (Person person : personList)
            if (person.name.startsWith("T") && person.age > 25 && person.hairColor == HairColor.BLACK && person.skinColor == SkinColor.WHITE)
                goodParents.add(person);
        return goodParents;
    }

    public List<GoodParents> findAllGoodMan(List<GoodParents> goodParents) {
        return goodParents.stream().filter(goodParent -> goodParent.sex == Gender.MALE)
                .collect(Collectors.toList());
    }

    public List<GoodParents> findAllGoodGirl(List<GoodParents> goodParents) {
        return goodParents.stream().filter(goodParent -> goodParent.sex == Gender.FEMALE)
                .collect(Collectors.toList());
    }
}
