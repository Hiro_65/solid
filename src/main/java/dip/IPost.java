package dip;

import single.responsibility.cohension.ex3.Database;

public interface IPost {
    void createPost(Database db, String postMessage);
    void createPost(Database db, String postMessage, int number);
}
