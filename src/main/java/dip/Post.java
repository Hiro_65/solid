package dip;

import single.responsibility.cohension.ex3.Database;

public class Post implements IPost {

    @Override
    public void createPost(Database db, String postMessage) {
        db.add(postMessage);
    }

    @Override
    public void createPost(Database db, String postMessage, int number) {
        db.add(postMessage);
    }
}
