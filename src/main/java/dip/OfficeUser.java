package dip;

import single.responsibility.cohension.ex3.Database;

public class OfficeUser {
    public void publishNewPost() {
        Database db = new Database();
        String postMessage = "example message";
        IPost post = new Post();
        post.createPost(db, postMessage);
        post.createPost(db, postMessage, 3);
    }
}
