package isp;

public class Dog implements Animal, Walkable {
    @Override
    public void walk() {
        System.out.println("dog can walk");
    }
}
