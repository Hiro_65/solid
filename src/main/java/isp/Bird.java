package isp;

public class Bird implements Animal, Flyable, Walkable {
    @Override
    public void fly() {
        System.out.println("bird can fly");
    }

    @Override
    public void walk() {
        System.out.println("bird can walk");
    }
}
